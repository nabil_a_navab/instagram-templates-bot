import os
import threading
import telebot
import time
from PIL import Image
from settings import TELEGRAM_TOKEN, LIQPAY_TEST_TOKEN, LIQPAY_LIVE_TOKEN
from GoogleDriveDownloader import GoogleDriveDownloader, SCOPES
from telebot.types import LabeledPrice, ShippingOption
from validate_email import validate_email


bot = telebot.TeleBot(TELEGRAM_TOKEN)

template_title = ''
folderID = ''
email_address = ''

prices = []
shipping_options = []

def check_photo(path_to_photo):
    try:
        Image.open(path_to_photo)
        return True
    except IOError:
        return False
    except:
        return False

def download_photos():
    gdd = GoogleDriveDownloader(SCOPES)
    folders = gdd.get_folders()

    for name, id in folders.items():

        files = gdd.get_content_of_folder(folder_id=id)
        file = {}

        #check even if file is photo
        for file_name, file_id in files.items():
            if file_name[-4:] == '.jpg':
                try:
                    name = name+'.jpg'
                    gdd.download_users_file(file_id=file_id, file_name=name)
                except:
                    name = name + '.jpg'
                    gdd.download_google_file(file_id, name)

                continue

@bot.message_handler(commands=['start'])
def send_welcome(message):
    user_murkup = telebot.types.ReplyKeyboardMarkup(True)
    user_murkup.row('Купить', 'Посмотреть шаблоны')

    bot.send_message(message.from_user.id, "Привет",reply_markup=user_murkup)

@bot.message_handler(content_types=['text'])
def menu(message):
    if message.text == 'Посмотреть шаблоны':
        for file in os.listdir(os.getcwd()+"/files"):
            print("FILE:", file)
            if file.endswith(".jpg"):
                bot.send_photo(message.from_user.id, caption="Имя шаблона: " + str(file[:-4]), photo=open(os.getcwd()+'/files/'+file, 'rb'))

        user_murkup = telebot.types.ReplyKeyboardMarkup(True)
        user_murkup.row('Назад')

        msg = bot.send_message(message.from_user.id, "Введите название вашего шаблона.", reply_markup=user_murkup)
        bot.register_next_step_handler(msg, send_template)

    elif message.text == 'Купить':
        user_murkup = telebot.types.ReplyKeyboardMarkup(True)
        user_murkup.row('Назад')

        msg = bot.send_message(message.from_user.id, "Введите название вашего шаблона.", reply_markup=user_murkup)
        bot.register_next_step_handler(msg, send_template)

    elif message.text == "Назад":
        user_murkup = telebot.types.ReplyKeyboardMarkup(True)
        user_murkup.row('Назад')

        msg = bot.send_message(message.from_user.id, "Введите название вашего шаблона.", reply_markup=user_murkup)
        bot.register_next_step_handler(msg, send_template)

    elif message.text == "Вызвать дизайнера":
        user_murkup = telebot.types.ReplyKeyboardMarkup(True)
        user_murkup.row('Купить', 'Посмотреть шаблоны')

        msg = bot.send_message(message.from_user.id, "Привет", reply_markup=user_murkup)
        bot.register_next_step_handler(msg, menu)

    else:
        user_murkup = telebot.types.ReplyKeyboardMarkup(True)
        user_murkup.row('/start')

        msg = bot.send_message(message.from_user.id, "Начните с начала.", reply_markup=user_murkup)
        bot.register_next_step_handler(msg, send_welcome)

def send_template(message):
    global template_title

    if message.text == "Назад":
        user_murkup = telebot.types.ReplyKeyboardMarkup(True)
        user_murkup.row('Купить', 'Посмотреть шаблоны')

        msg = bot.send_message(message.from_user.id, "Привет", reply_markup=user_murkup)
        bot.register_next_step_handler(msg, menu)
    else:
        try:
            template_title = message.text
            path_to_file = os.getcwd() + '/files/' + message.text + '.jpg'

            user_murkup = telebot.types.ReplyKeyboardMarkup(True)
            user_murkup.row('Да', "Нет", "Вызвать дизайнера", 'Назад')

            msg = bot.send_photo(message.from_user.id, caption="Это ваш шаблон?", photo=open(path_to_file, 'rb'),
                                 reply_markup=user_murkup)
            bot.register_next_step_handler(msg, check_template)
        except Exception as e:

            user_murkup = telebot.types.ReplyKeyboardMarkup(True)
            user_murkup.row('Назад')

            msg = bot.send_message(message.from_user.id,
                                   "Такового шаблона не существует. Введите еще раз название шаблона.",
                                   reply_markup=user_murkup)
            bot.register_next_step_handler(msg, menu)
            print(e)
        except:
            print("Error!!!")

def check_template(message):
    if message.text == "Да":
        global prices, shipping_options, folderID

        gdd = GoogleDriveDownloader(SCOPES)
        files = gdd.get_files()

        file = None

        for file_name, file_id in files.items():
            if file_name == template_title:
                file = {file_name: file_id}

                folderID = file_id
                break

        if file is not None:
            prices = [LabeledPrice(label=template_title, amount=350 * 100)]
            shipping_options = [ShippingOption(id=template_title, title=template_title).add_price(
                LabeledPrice(label=template_title, amount=350 * 100))]

            user_murkup = telebot.types.ReplyKeyboardMarkup(True)
            user_murkup.row('Назад')

            msg = bot.send_message(message.from_user.id,
                                   "Введите ваш электронный ящик для получения доступа к файлам шаблона.", reply_markup=user_murkup)
            bot.register_next_step_handler(msg, check_email)
        else:
            user_murkup = telebot.types.ReplyKeyboardMarkup(True)
            user_murkup.row('/start')

            msg = bot.send_message(message.from_user.id,
                                   "Подобного шаблона не существует. Чтобы начать заново, нажмите /start.",
                                   reply_markup=user_murkup)
            bot.register_next_step_handler(msg, send_welcome)

    elif message.text == "Нет":
        user_murkup = telebot.types.ReplyKeyboardMarkup(True)
        user_murkup.row('Посмотреть шаблоны')

        msg = bot.send_message(message.from_user.id, "Смотрите каталог наших товаров", reply_markup=user_murkup)
        bot.register_next_step_handler(msg, menu)

    elif message.text == "Вызвать дизайнера":
        user_murkup = telebot.types.ReplyKeyboardMarkup(True)
        user_murkup.row('Посмотреть шаблоны')

        msg = bot.send_message(message.from_user.id, 'Дизайнер @desfran', reply_markup=user_murkup)
        bot.register_next_step_handler(msg, menu)

def check_email(message):
    global email_address
    if message.text == "Назад":
        user_murkup = telebot.types.ReplyKeyboardMarkup(True)
        user_murkup.row('Купить', 'Посмотреть шаблоны', "Вызвать дизайнера")

        msg = bot.send_message(message.from_user.id, "Привет", reply_markup=user_murkup)
        bot.register_next_step_handler(msg, menu)
    else:
        email = message.text

        is_valid = validate_email(email)

        # check even if entered email
        if is_valid:

            email_address = email
            user_murkup = telebot.types.ReplyKeyboardMarkup(True)
            user_murkup.row('Купить шаблон')

            msg = bot.send_message(message.from_user.id,
                                   "Предлагаем купить его у нас.",
                                   reply_markup=user_murkup)
            bot.register_next_step_handler(msg, command_pay)

        else:
            user_murkup = telebot.types.ReplyKeyboardRemove(True)
            user_murkup.row('Назад')

            msg = bot.send_message(message.from_user.id,
                                   "Вы ввели неверный адрес электронной почты. Введите пожалуйста снова.",
                                   reply_markup=user_murkup)
            bot.register_next_step_handler(msg, check_email)

@bot.message_handler(content_types=['text'])
def command_pay(message):

    if message.text == 'Купить шаблон':

        bot.send_invoice(message.chat.id,
                         title=template_title,
                         description='Template name: {}'.format(template_title),
                         provider_token=LIQPAY_TEST_TOKEN,
                         currency='uah',
                         photo_url='https://images.unsplash.com/photo-1519873174361-37788c5a73c7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
                         photo_height=512,  # !=0/None or picture won't be shown
                         photo_width=512,
                         photo_size=512,
                         is_flexible=False,  # True If you need to set up Shipping Fee
                         prices=prices,
                         invoice_payload = 'Have a nice day',
                         start_parameter='instagram-templates')

@bot.shipping_query_handler(func=lambda query: True)
def shipping(shipping_query):
    print(shipping_query)
    bot.answer_shipping_query(shipping_query.id, ok=True, shipping_options=shipping_options,
                              error_message='Oh, seems like our Dog couriers are having a lunch right now. Try again later!')

@bot.pre_checkout_query_handler(func=lambda query: True)
def checkout(pre_checkout_query):
    bot.answer_pre_checkout_query(pre_checkout_query.id, ok=True,
                                  error_message="Aliens tried to steal your card's CVV, but we successfully protected your credentials,"
                                                " try to pay again in a few minutes, we need a small rest.")

@bot.message_handler(content_types=['successful_payment'])
def got_payment(message):
    th = threading.Thread(target=download_photos)
    th.start()

    gdd = GoogleDriveDownloader(SCOPES)
    gdd.create_permission(folderID, email_address)

    link = 'https://drive.google.com/drive/u/0/folders/' + folderID

    user_murkup = telebot.types.ReplyKeyboardMarkup(True)
    user_murkup.row('/start')

    msg = bot.send_message(message.from_user.id, "Оплата прошла успешно. Ссылка на ваш шаблон {} Если это не ваш шаблон, или у вас возникли проблемы другого типа обратитесь пожалуйста к оператору @desfran".format(link), reply_markup=user_murkup)

    bot.register_next_step_handler(msg, send_welcome)

bot.skip_pending = True

while True:

    try:

        bot.polling(none_stop=True)

    # ConnectionError and ReadTimeout because of possible timout of the requests library

    # TypeError for moviepy errors

    # maybe there are others, therefore Exception

    except Exception as e:


        time.sleep(15)
